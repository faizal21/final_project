# Final Project #

## Kelompok 8 ##

## Anggota Kelompok ##

* Muhammad Ardi Nur Syamsu
* Faizal Arafat
* Muhammad Reza Harsono

## Tema Project ##

Forum Tanya Jawab

## ERD ##

<img src='ERD.jpg'>

## Link Video

Link Demo Aplikasi : <a href="https://youtu.be/HStOMgCx3R4">https://youtu.be/HStOMgCx3R4</a>

Link Deploy : <a href="http://tugas-akhir-kel8.herokuapp.com/">http://tugas-akhir-kel8.herokuapp.com/</a>
