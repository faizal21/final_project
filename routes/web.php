<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\TopikController;
use App\Http\Controllers\KomentarController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::resource('kategori', KategoriController::class)->middleware('auth');

Route::resource('profil', ProfilController::class)->middleware('auth');

Route::resource('topik', TopikController::class)->middleware('auth');

Route::resource('komentar', KomentarController::class)->middleware('auth');

Route::get('tinymce', function(){
    return view('tinymce.index');
});