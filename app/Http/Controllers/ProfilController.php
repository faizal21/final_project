<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\profile;

class ProfilController extends Controller
{
    //
    public function index(){
        $user = Auth::user();

        //$user = User::where('email', '=', Input::get('email'))->first();
        $profil = profile::where('user_id', '=', $user->id)->first();

        if ($profil === null){
            return view('profil.new');
        }


        return view('profil.index', compact('profil'));
    }

    public function create(){
        return view('profil.new');
    }

    public function edit($id){
        $profil = profile::where('user_id', '=', $id)->first();
        return view('profil.edit', compact('profil'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => ['required', 'max:999', 'numeric'],
            'bio' => ['required'],
            'alamat' => ['required']
        ]);
        profile::find($id)->update([
            'umur' => $request->umur,
            'bio' => $request->bio,
            'alamat' => $request->alamat
        ]);

        return redirect('profil')->with('success', 'Profil berhasil diupdate ...');
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'umur' => ['required', 'max:999', 'numeric'],
            'bio' => ['required'],
            'alamat' => ['required']
        ]);

        print_r($request->umur);
        print_r($request->bio);
        print_r($request->alamat);
        
        profile::create([
            'umur' => $request->umur,
            'bio' => $request->bio,
            'alamat' => $request->alamat,
            'user_id' => $user->id
        ]);
        return redirect('profil')->with('success', 'Profil berhasil dibuat');
    }

    public function destroy($id)
    {
        profil::find($id)->delete();
        return redirect()->back()->with('success', 'Profil telah dihapus !!');
    }
}

