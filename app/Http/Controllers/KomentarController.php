<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Komentar;
use App\Models\Topik;


class KomentarController extends Controller
{
    //
    public function store(Request $request){
        $request->validate([
            'komentar' => ['required', 'max:1028']
        ]);

        $data = $request->all();
        $user = Auth::user();
        
        Komentar::create([
            'komentar' => $data['komentar'],
            'topik_id' => $data['topik_id'],
            'user_id' => $user->id
        ]);

        return redirect(route('topik.show', $data['topik_id']))->with('success', 'Komentar berhasil ditambahkan');
    }

    public function edit($id){
        $data = Komentar::find($id);
        return view('komentar.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'komentar' => ['required', 'max:1028']
        ]);

        $data = $request->all();

        Komentar::find($id)->update([
            'komentar' => $data['komentar']
        ]);

        return redirect(route('topik.show', $data['topik_id']))->with('success', 'Komentar berhasil diubah');
    }

    public function destroy($id)
    {
        Komentar::find($id)->delete();
        return redirect()->back()->with('success', 'Komentar telah Terhapus !!');
    }
}
