<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Topik;
use Illuminate\Http\Request;

class KategoriController extends Controller
{

    public function index()
    {
        $kategori = Kategori::all();
        return view('kategori.index', ['data' => $kategori]);
    }


    public function create()
    {
        return view('kategori.new');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'max:255', 'unique:kategori']
        ]);
        Kategori::create([
            'nama' => $request->nama
        ]);

        return redirect('kategori')->with('success', 'Kategori berhasil ditambahkan ...');
    }

    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.edit', [
            'data' => $kategori
        ]);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'max:255', 'unique:kategori']
        ]);
        Kategori::find($id)->update([
            'nama' => $request->nama
        ]);

        return redirect('kategori')->with('success', 'Kategori berhasil diupdate ...');
    }
    
    public function show($id){
        $kategori = Kategori::find($id);
        $topik = Topik::where('kategori_id', '=', $id)->get();
        return view('kategori.show', compact('topik', 'kategori'));
    }


    public function destroy($id)
    {
        Kategori::find($id)->delete();
        return redirect()->back()->with('success', 'Kategori Berhasil Terhapus !!');
    }

    
}
