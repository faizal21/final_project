<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Topik;
use App\Models\Komentar;

use Image;


class TopikController extends Controller
{
    public function index(){
        $data = Topik::all();
        return view('topik.index', compact('data'));
    }
    //
    public function create(){
        $kategori = Kategori::all();
        return view('topik.new', compact('kategori'));
    }

    public function store(Request $request)
    {        
        $request->validate([
            'judul' => ['required', 'max:255'],
            'pertanyaan' => ['required'],
            'gambar' => ['required'],
            'kategori' => ['required']
        ]);

        $image = $request->file('gambar');
        $filename= date('YmdHi').$image->getClientOriginalName();
        $thumbImage = Image::make($image->getRealPath());
        
        $height = $thumbImage->height();
        $width = $thumbImage->width();

        if ($height < $width){
            $thumbImage->resize(($width/($height/200)), 200);
        } else {
            $thumbImage->resize(200, ($height/($width/200)));
        }

        $thumbPath = public_path() . '/image/' . $filename;
        $thumbImage->crop(200, 200);
        $thumbImage = Image::make($thumbImage)->save($thumbPath);

        //$file-> move(public_path('image'), $filename);
        //print_r($request->all());
        //print_r($file);
        $user = Auth::user();
        
        Topik::create([
            'judul' => $request->judul,
            'pertanyaan' => $request->pertanyaan,
            'gambar' => $filename,
            'kategori_id' => $request->kategori,
            'user_id' => $user->id
        ]);

        return redirect('topik')->with('success', 'Topik berhasil ditambahkan ...');
    }

    public function edit($id)
    {
        $topik = Topik::find($id);
        $kategori = Kategori::all();
        return view('topik.edit', compact('topik', 'kategori'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => ['required', 'max:255'],
            'pertanyaan' => ['required'],
            'kategori' => ['required']
        ]);

        Topik::find($id)->update([
            'judul' => $request->judul,
            'pertanyaan' => $request->pertanyaan,
            'kategori_id' => $request->kategori,
        ]);

        return redirect('topik')->with('success', 'Topik berhasil diupdate ...');
    }

    public function show($id){
        $komentar = Komentar::where('topik_id', '=', $id)->get();;
        $topik = Topik::find($id);
        return view('topik.show', compact('topik', 'komentar'));
    }

    public function destroy($id)
    {
        Topik::find($id)->delete();
        return redirect()->back()->with('success', 'Topik Berhasil Terhapus !!');
    }

}
