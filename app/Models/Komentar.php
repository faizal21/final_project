<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Topik;
use App\Models\User;

class Komentar extends Model
{
    use HasFactory;
    protected $table = 'komentar';
    protected $fillable = ['komentar', 'topik_id', 'user_id'];

    public function topik(){
        return $this->belongsTo(Topik::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
