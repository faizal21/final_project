<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Kategori;
use App\Models\Komentar;

class Topik extends Model
{
    use HasFactory;
    protected $table = "topik";

    protected $fillable = ['judul', 'pertanyaan', 'gambar', 'kategori_id', 'user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function kategori(){
        return $this->belongsTo(Kategori::class);
    }

    public function komentar(){
        return $this->hasMany(Komentar::class);
    }


}
