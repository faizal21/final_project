<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class profile extends Model
{
    protected $table = "profil";

    protected $fillable = ["umur", "bio", "alamat", "user_id"];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
