@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Buat Profile</h1>
            </section>
            <section class="mdc-card__supporting-text">
                <div class="mdc-layout-grid__inner">
                    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                        <form action={{ route('profil.store') }} method="POST">
                            @csrf
                            <div class="template-demo">
                                <div id="demo-tf-box-leading-wrapper">
                                    <div class="mdc-text-field w-100">
                                        <input type="text" id="umur" class="mdc-text-field__input" name="umur">
                                        <label class="mdc-floating-label" for="my-text-field">Umur</label>
                                        <div class="mdc-line-ripple"></div>
                                    </div>
                                    @error('umur')
                                        <p>
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>
                                <div id="demo-tf-box-leading-wrapper">
                                    <div class="mdc-text-field w-100">
                                        <input type="text" id="bio" class="mdc-text-field__input" name="bio">
                                        <label class="mdc-floating-label" for="my-text-field">Bio</label>
                                        <div class="mdc-line-ripple"></div>
                                    </div>
                                    @error('bio')
                                        <p>
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>
                                <div id="demo-tf-box-leading-wrapper">
                                    <div class="mdc-text-field w-100">
                                        <input type="text" id="alamat" class="mdc-text-field__input" name="alamat">
                                        <label class="mdc-floating-label" for="my-text-field">Alamat</label>
                                        <div class="mdc-line-ripple"></div>
                                    </div>
                                    @error('alamat')
                                        <p>
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>

                                <button type="submit" class="mdc-button mdc-button--raised" data-mdc-auto-init="MDCRipple">
                                    Simpan
                                </button>
                                <a href={{ route('kategori.index') }}
                                    class="mdc-button mdc-button--raised secondary-filled-button"
                                    data-mdc-auto-init="MDCRipple">
                                    Kembali
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
