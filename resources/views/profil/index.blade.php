@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Kategori</h1>
                <hr>
                <a href={{ route('profil.edit', $profil->user_id) }} class="mdc-button mdc-button--raised" data-mdc-auto-init="MDCRipple">
                    Update Profile
                </a>
            </section>
            <div>
                <table class="table table-hoverable">
                    <thead>
                        <tr>
                            <th width="20%" class="text-left"><h4>Data</h4></th>
                            <th width="80%" class="text-left"><h4>Profil</h4></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-left">Nama</td><td class="text-left">{{ $profil->user->name }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">Email</td><td class="text-left">{{ $profil->user->email }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">Umur</td><td class="text-left">{{ $profil->umur }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">Bio</td><td class="text-left">{{ $profil->bio }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">Alamat</td><td class="text-left">{{ $profil->alamat }}</td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
