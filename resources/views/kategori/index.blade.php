@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Kategori</h1>
                <hr>
                <a href={{ route('kategori.create') }} class="mdc-button mdc-button--raised" data-mdc-auto-init="MDCRipple">
                    Tambah Kategori
                </a>
            </section>
            <div>
                <table class="table table-hoverable">
                    <thead>
                        <tr>
                            <th width="15%" class="text-left">No</th>
                            <th width="60%" class="text-left">Kategori</th>
                            <th width="25%" class="text-left"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 0;
                        @endphp
                        @forelse ($data as $row)
                            @php
                                $no = $no + 1;
                            @endphp
                            <tr>
                                <td class="text-left">{{ $no }}</td>
                                <td class="text-left"><a href="{{ route('kategori.show', $row->id)}}">{{ $row->nama }}</a></td>
                                <td class="text-left">
                                    <div class="d-flex">
                                        <a class="mdc-button mdc-button--raised" href={{ route('kategori.edit', $row->id) }}
                                            data-mdc-auto-init="MDCRipple">Edit</a>
                                        <form class="pl-1" action={{ route('kategori.destroy', $row->id) }}
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="id" value="{{ $row->id }}">
                                            <button class="mdc-button mdc-button--raised secondary-filled-button"
                                                type="submit" data-mdc-auto-init="MDCRipple">Hapus</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">Data kosong</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
