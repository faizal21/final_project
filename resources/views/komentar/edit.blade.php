@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Edit Komentar di {{ $data->topik->judul }}</h1>
            </section>
            <section class="mdc-card__supporting-text">
                <div class="mdc-layout-grid__inner">
                    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                        <form action={{ route('komentar.update', $data->id) }} method="POST">
                            @csrf
                            @method('PUT')
                            <div class="template-demo">
                                <div id="demo-tf-box-leading-wrapper">
                                    <div id="tf-box-leading-example" class="mdc-text-field mdc-text-field--box w-100">
                                        <input type="hidden" value="{{ $data->topik->id}}" name="topik_id">
                                        <input type="text" id="nama" name="komentar" class="mdc-text-field__input"
                                            required value="{{ $data->komentar }}">
                                        <label for="nama" class="mdc-text-field__label">Komentar</label>
                                        <div class="mdc-text-field__bottom-line"></div>
                                    </div>
                                    @error('nama')
                                        <p>
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>
                                <button type="submit" class="mdc-button mdc-button--raised" data-mdc-auto-init="MDCRipple">
                                    Update
                                </button>
                                <a href={{ route('topik.show', $data->topik_id) }}
                                    class="mdc-button mdc-button--raised secondary-filled-button"
                                    data-mdc-auto-init="MDCRipple">
                                    Kembali
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
