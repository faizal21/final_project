@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Topik</h1>
                <hr>
                <a href={{ route('topik.create') }} class="mdc-button mdc-button--raised" data-mdc-auto-init="MDCRipple">
                    Tambah Topik
                </a>
            </section>
            <div>
                <table class="table table-hoverable">
                    <thead>
                        <tr>
                            <th width="15%" class="text-left">No</th>
                            <th width="60%" class="text-left">Judul</th>
                            <th width="25%" class="text-left"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 0;
                            $public_folder = public_path();
                        @endphp
                        @forelse ($data as $row)
                            @php
                                $no = $no + 1;
                            @endphp
                            <tr>
                                <td class="text-left">{{ $no }}</td>
                                <td class="text-left"><a href="{{ route('topik.show', $row->id)}}">{{ $row->judul }}</a></td>
                                <td class="text-left">
                                    <div class="d-flex">
                                        @if ($row->user->id == Auth::user()->id)
                                        <a class="mdc-button mdc-button--raised" href={{ route('topik.edit', $row->id) }}
                                            data-mdc-auto-init="MDCRipple">Edit</a>
                                        <form class="pl-1" action={{ route('topik.destroy', $row->id) }}
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="id" value="{{ $row->id }}">
                                            <button class="mdc-button mdc-button--raised secondary-filled-button"
                                                type="submit" data-mdc-auto-init="MDCRipple">Hapus</button>
                                        </form>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-left"><img src="{{ asset('image/'.$row->gambar)}}" height="100"></td>
                                <td class="text-left">{{ $row->pertanyaan }}</td>
                                @php
                                @endphp
                                <td class="text-left"></td> 
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">Data kosong</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
