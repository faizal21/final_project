@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Kategori {{ $topik->kategori->nama }}</h1>
                <hr>
                Topik {{ $topik->judul}}
            </section>
            <div>
                <table class="table table-hoverable">
                    <thead>
                        <tr>
                            <th width="60%" class="text-left">Thread</th>
                            <th width="25%" class="text-left"></th>
                            <th width="15%" class="text-left"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan=2 class="text-left">{{ $topik->pertanyaan }}</td>
                            <td class="text-left"><img src="{{ asset('image/'.$topik->gambar)}}" height="100"></td>
                        </tr>
                        
                        @forelse ($komentar as $row)
                            <tr>
                                <td class="text-left">
                                    {{ $row->komentar }}
                                </td>
                                <td class="text-left">
                                    Komentar oleh <br> {{ $row->user->name}}
                                </td>
                                <td class="text-left">
                                @if ($row->user->id == Auth::user()->id)
                                        <a class="mdc-button mdc-button--raised" href={{ route('komentar.edit', $row->id) }}
                                            data-mdc-auto-init="MDCRipple">Edit</a>
                                        <form class="pl-1" action={{ route('komentar.destroy', $row->id) }}
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="id" value="{{ $row->id }}">
                                            <button onclick="deleteConfirm('delele-product-form-39')" class="mdc-button mdc-button--raised secondary-filled-button"
                                                type="submit" data-mdc-auto-init="MDCRipple">Hapus</button>
                                        </form>
                                @endif
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                            </td>
                        </tr>
                        <tr>
                            @auth
                            <td colspan=3>
                            <form method="POST" action="{{route('komentar.store')}}">
                                @csrf
                                <label class="mdc-floating-label" for="my-text-field">Tambah Komentar</label>
                                <input type="hidden" name="topik_id" value={{ $topik->id }}>
                                <input type=text name="komentar" class="mdc-text-field__input">
                                <button type="submit" class="mdc-button mdc-button--raised">Tambah</button>
                                <button type="reset" class="mdc-button mdc-button--raised secondary-filled-button">Reset</button>
                            </form>
                            </td>
                            @endauth
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
