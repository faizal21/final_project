@extends('layouts.template')
@section('content')
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
        <div class="mdc-card">
            <section class="mdc-card__primary">
                <h1 class="mdc-card__title mdc-card__title--large">Edit Topik</h1>
            </section>
            <section class="mdc-card__supporting-text">
                <div class="mdc-layout-grid__inner">
                    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                        <form action={{ route('topik.update', $topik->id) }} method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="template-demo">
                                <div id="demo-tf-box-leading-wrapper">
                                    <div class="mdc-text-field w-100">
                                        <input type="text" id="umur" class="mdc-text-field__input" name="judul" value="{{$topik->judul}}"">
                                        <label class="mdc-floating-label" for="my-text-field">Judul</label>
                                        <div class="mdc-line-ripple"></div>
                                    </div>
                                    @error('judul')
                                        <p>
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>
                                <div id="demo-tf-box-leading-wrapper">
                                    <div class="mdc-text-field w-100">
                                        <input type="text" id="bio" class="mdc-text-field__input" name="pertanyaan" value="{{$topik->pertanyaan}}"">
                                        <label class="mdc-floating-label" for="my-text-field">Pertanyaan</label>
                                        <div class="mdc-line-ripple"></div>
                                    </div>
                                    @error('topik')
                                        <p>
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>
                                <div id="demo-tf-box-leading-wrapper">
                                    <br>
                                    <select name="kategori">
                                    @forelse ($kategori as $kat)
                                        <option value="{{$kat->id}}">{{$kat->nama}}</option>
                                    @empty
                                        <option value="null">Belum ada kategori</option>
                                    @endforelse
                                    </select><br>
                                    <label for="kategori">Kategori</label>
                                </div>
                                

                                <button type="submit" class="mdc-button mdc-button--raised" data-mdc-auto-init="MDCRipple">
                                    Simpan
                                </button>
                                <a href={{ route('topik.index') }}
                                    class="mdc-button mdc-button--raised secondary-filled-button"
                                    data-mdc-auto-init="MDCRipple">
                                    Kembali
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection