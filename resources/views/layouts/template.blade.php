<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Material Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('template/node_modules/mdi/css/materialdesignicons.min.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('/template/css/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('/template/images/favicon.png') }}" />
</head>

<body>
    <div class="body-wrapper">
        <!-- Sidebar -->
        @include('partial.sidebar')
        <!-- Sidebar -->

        <!-- Navbar -->
        @include('partial.navbar')
        <!-- Navbar -->


        <div class="page-wrapper mdc-toolbar-fixed-adjust">
            <main class="content-wrapper">
                <div class="mdc-layout-grid">
                    <div class="mdc-layout-grid__inner">
                        @yield('content')
                    </div>
                </div>
            </main>
        </div>
    </div>
    <!-- body wrapper -->
    <!-- plugins:js -->
    <script src="{{ asset('/template/node_modules/material-components-web/dist/material-components-web.min.js') }}">
    </script>
    <script src="{{ asset('/template/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('/template/js/misc.js') }}"></script>
    <script src="{{ asset('/template/js/material.js') }}"></script>
    @include('sweetalert::alert')
    <!-- endinject -->
    <!-- Custom js for this page-->
    <!-- End custom js for this page-->
</body>

</html>
