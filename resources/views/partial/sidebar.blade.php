<aside class="mdc-persistent-drawer mdc-persistent-drawer--open">
    <nav class="mdc-persistent-drawer__drawer">
        <div class="mdc-persistent-drawer__toolbar-spacer">
            <a href="/" class="brand-logo">
                <img src="{{ asset('/template/images/logo.svg') }}" alt="logo">
            </a>
        </div>
        <div class="mdc-list-group">
            <nav class="mdc-list mdc-drawer-menu">
                @auth
                    <div class="mdc-list-item mdc-drawer-item purchase-link">

                        <a href="javascript:void(0)" class="mdc-button mdc-button--raised mdc-button--dense mdc-drawer-link"
                            data-mdc-auto-init="MDCRipple">
                            {{ Auth::user()->name }}
                        </a>
                    </div>
                @endauth
                <div class="mdc-list-item mdc-drawer-item">
                    <a class="mdc-drawer-link" href={{ url('/') }}>
                        <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon"
                            aria-hidden="true">desktop_mac</i>
                        Dashboard
                    </a>
                </div>
                @auth
                    <div class="mdc-list-item mdc-drawer-item">
                        <a class="mdc-drawer-link" href="{{route('topik.index')}}">
                            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon"
                                aria-hidden="true">question_answer</i>
                            Ajukan Pertanyaan
                        </a>
                    </div>
                    <div class="mdc-list-item mdc-drawer-item" data-toggle="expansionPanel" target-panel="ui-sub-menu">
                        <a class="mdc-drawer-link" href={{ route('kategori.index') }}>
                            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon"
                                aria-hidden="true">dashboard</i>
                            Kategori
                        </a>
                    </div>
                    <div class="mdc-list-item mdc-drawer-item" data-toggle="expansionPanel" target-panel="ui-sub-menu">
                        <a class="mdc-drawer-link" href='/tinymce'>
                            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon"
                                aria-hidden="true">event_note</i>
                            Contoh TinyMCE
                        </a>
                    </div>
                    <div class="mdc-list-item mdc-drawer-item" href="#" data-toggle="expansionPanel"
                        target-panel="sample-page-submenu">
                        <div class="mdc-expansion-panel" id="sample-page-submenu">
                            <nav class="mdc-list mdc-drawer-submenu">
                                <div class="mdc-list-item mdc-drawer-item">
                                    <a class="mdc-drawer-link" href="">
                                        Blank Page
                                    </a>
                            </nav>
                        </div>
                    </div>
                @endauth
            </nav>
        </div>
    </nav>
</aside>
