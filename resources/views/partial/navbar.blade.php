<header class="mdc-toolbar mdc-elevation--z4 mdc-toolbar--fixed">
    <div class="mdc-toolbar__row">
        <section class="mdc-toolbar__section mdc-toolbar__section--align-start">
            <a href="#" class="menu-toggler material-icons mdc-toolbar__menu-icon">menu</a>
            <span class="mdc-toolbar__input">
                <div class="mdc-text-field">
                    <input type="text" class="mdc-text-field__input" id="css-only-text-field-box"
                        placeholder="Search anything...">
                </div>
            </span>
        </section>
        <section class="mdc-toolbar__section mdc-toolbar__section--align-end" role="toolbar">
            <div class="mdc-menu-anchor">
            </div>
            <div class="mdc-menu-anchor mr-1">
                <a href="#" class="mdc-toolbar__icon toggle mdc-ripple-surface" data-toggle="dropdown"
                    toggle-dropdown="logout-menu" data-mdc-auto-init="MDCRipple">
                    <i class="material-icons">more_vert</i>
                </a>
                <div class="mdc-simple-menu mdc-simple-menu--right" tabindex="-1" id="logout-menu">
                    <ul class="mdc-simple-menu__items mdc-list" role="menu" aria-hidden="true">
                        {{-- Login --}}
                        @guest
                            <li class="mdc-list-item" role="menuitem" tabindex="0">
                                <a href="/login">
                                    <i class="material-icons mdc-theme--primary mr-1">power_settings_new</i>
                                    Login
                                </a>
                            </li>
                        @endguest

                        {{-- Logout --}}
                        @auth
                            <li class="mdc-list-item" role="menuitem" tabindex="0">
                                <a href="{{ route('profil.index') }}">
                                    <i class="material-icons mdc-theme--primary mr-1">settings</i>
                                    Profile
                                </a>
                            </li>
                            <li class="mdc-list-item" role="menuitem" tabindex="0">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="material-icons mdc-theme--primary mr-1">power_settings_new</i>
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </section>
    </div>
</header>
